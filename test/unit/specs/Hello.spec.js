import Vue from 'vue'
import Hello from '@/components/Hello'
import Revert from '@/components/RevertMsg'

describe('Hello.vue', () => {
  const Constructor = Vue.extend(Hello)
  const vm = new Constructor().$mount()
  it('should render correct contents', () => {
    expect(vm.$el.querySelector('.hello h1').textContent)
      .to.equal('Welcome to Your Vue.js App')
  })
  it('should have the correct datas', () => {
    expect(vm.$data.msg).to.equal('Welcome to Your Vue.js App')
  })
})

describe('RevertMsg.vue', () => {
  const Constructor = Vue.extend(Revert)
  const vm = new Constructor().$mount()
  vm.$data.msg = 'test'
  it('should render the message in reverse', () => {
    expect(vm.reverseMsg).to.equal('tset')
  })
})
